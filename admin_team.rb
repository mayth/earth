get '/admin/team/create' do
  @page_title = 'Create Team'
  admin_layout :admin_team_create
end

post '/admin/team/create' do
  begin
    raise "Unacceptable character in the team name." unless params['name'] =~ %r!^[/\]\[a-zA-Z0-9_-]+$!
    @team = Team.create(params['name'])
  rescue
    halt "Error! #{$!}"
  end
  @page_title = 'Team Created'
  admin_layout :admin_team_created
end

get '/admin/team/list' do
  @teams = Team.all

  @page_title = 'List of Teams'
  admin_layout :admin_team_list
end

get '/admin/team/:team_id' do
  team_id = Integer(params[:team_id]) rescue -1
  @team = Team.find(id: team_id)
  halt "Team ID: #{team_id} was not found." unless @team
  @page_title = "#{@team.name}"
  admin_layout :admin_team_individual
end

get '/admin/team/suspend/:team_id' do
  team_id = Integer(params[:team_id]) rescue -1
  @team = Team.find(id: team_id)
  halt "Team ID: #{team_id} was not found." unless @team
  @page_title = "Suspending #{@team.name}"
  admin_layout :admin_team_suspend
end

post '/admin/team/suspend/:team_id' do
  team_id = Integer(params[:team_id]) rescue -1
  team = Team.find(id: team_id)
  halt "Team ID: #{team_id} was not found." unless team
  begin
    suspend_until = Time.iso8601(params['time']) unless params['time'].empty?
  rescue ArgumentError
    halt "The given date is not valid"
  end
  team.suspend(suspend_until)

  redirect "/admin/team/#{team.id}"
end

post '/admin/team/resume/:team_id' do
  content_type :json
  team_id = Integer(params[:team_id]) rescue -1
  team = Team.find(id: team_id)
  halt ({result: 'failed', reason: "Team ID: #{team_id} was not found."}.to_json) unless team
  begin
    team.resume
  rescue
    halt ({result: 'failed', reason: "Something went wrong: #{$!}"}.to_json)
  end
  {result: 'success'}.to_json
end

post '/admin/team/delete/:team_id' do
  team_id = Integer(params[:team_id]) rescue -1
  team = Team.find(id: team_id)
  halt ({result: 'failed', reason: "Team ID: #{team_id} was not found."}.to_json) unless team
  team_id = team.id
  team_name = team.name
  team.scorelogs.each {|log| log.destroy}
  team.destroy

  content_type :json
  {result: 'success', team_name: team_name}.to_json
end
