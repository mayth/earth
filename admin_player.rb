get '/admin/player/create' do
  @page_title = 'Create New Player'
  admin_layout :admin_player_create
end

post '/admin/player/create' do
  team = Team.find(name: params['team_name'])
  halt 'A valid team is required. (A team name is not specified or the specified team does not exist.)' unless team
  @player = Player.create(params['name'], params['password'], team)

  admin_layout :admin_player_created
end

get '/admin/player/list' do
  @players = Player.all

  @page_title = 'List of Players'
  admin_layout :admin_player_list
end

get '/admin/player/:player_id' do
  player_id = Integer(params[:player_id]) rescue -1
  @player = Player.find(id: player_id)
  halt "Player ID: #{player_id} was not found." unless @player

  @page_title = "#{@player.name}"
  admin_layout :admin_player_individual
end

get '/admin/player/adjust_point/:player_id' do
  player_id = Integer(params[:player_id]) rescue -1
  @player = Player.find(id: player_id)
  halt "Player (ID: #{player_id}) is not found." unless @player
  @nonce = Digest::SHA2.hexdigest("#{Time.now.to_s}#{rand}")
  session['adm_adjust_nonce'] = @nonce

  @page_title = "Score Adjustment for #{@player.name}"
  admin_layout :admin_player_adjust
end

post '/admin/player/adjust_point/:player_id' do
  player_id = Integer(params[:player_id]) rescue -1
  @player = Player.find(id: player_id)
  halt "Player (ID:#{player_id} is not found." unless @player
  sess_nonce = session['adm_adjust_nonce']
  form_nonce = params['nonce']
  halt 'Oops.' if sess_nonce != form_nonce
  begin
    value = params['value'].to_i
    event = params['event']
    reason = params['reason'].empty? ? nil : params['reason']
    @player.adjust_point(value, event, reason)
  rescue
    halt "Unexpected Error: #{$!}"
  end

  admin_layout :admin_player_adjusted
end

get '/admin/player/suspend/:player_id' do
  player_id = Integer(params[:player_id]) rescue -1
  @player = Player.find(id: player_id)
  halt "Player ID: #{player_id} was not found." unless @player
  @page_title = "Suspending #{@player.name}"
  @page_subtitle = "Suspending Player"
  admin_layout :admin_player_suspend
end

post '/admin/player/suspend/:player_id' do
  player_id = Integer(params[:player_id]) rescue -1
  player = Player.find(id: player_id)
  halt "Player ID: #{player_id} was not found." unless player
  begin
    suspend_until = Time.iso8601(params['time']) unless params['time'].empty?
  rescue ArgumentError
    halt "The given date is not valid"
  end
  player.suspend(suspend_until)

  redirect "/admin/player/#{player.id}"
end

post '/admin/player/resume/:player_id' do
  content_type :json
  player_id = Integer(params[:player_id]) rescue -1
  player = Player.find(id: player_id)
  halt ({result: 'failed', reason: "Player ID: #{player_id} was not found."}.to_json) unless player
  begin
    player.resume
  rescue
    halt ({result: 'failed', reason: "Something went wrong: #{$!}"}.to_json)
  end
  {result: 'success'}.to_json
end

post '/admin/player/delete/:player_id' do
  content_type :json
  player_id = Integer(params[:player_id]) rescue -1
  player = Player.find(id: player_id)
  halt ({result: 'failed', reason: "Player ID: #{player_id} was not found."}.to_json) unless player
  begin
    player.trials.each {|t| t.destroy}
    player.scorelogs.each {|t| t.destroy}
    player.destroy
  rescue
    halt ({result: 'failed', reason: "Something went wrong: #{$!}"}.to_json)
  end
  {result: 'success'}.to_json
end

get '/admin/player/treat_as/:player_id' do
  player_id = Integer(params[:player_id]) rescue -1
  @player = Player.find(id: player_id)
  halt "Player ID:#{player_id} is not found." unless @player
  admin_layout :admin_player_challenge_adjust
end

post '/admin/player/treat_as/:player_id' do
  content_type :json
  player_id = Integer(params[:player_id]) rescue -1
  player = Player.find(id: player_id)
  halt "Player ID:#{player_id} is not found" unless player
  challenge_id = Integer(params[:challenge]) rescue -1
  challenge = Challenge.find(id: challenge_id)
  halt "Challenge ID:#{challenge_id} is not found." unless challenge
  flag = params['flag']
  flag = 'DUMMY_FLAG' if flag.empty?
  begin
    trial = Trial.new(
      submitted_flag: params['flag'],
      is_accepted: true,
      submitted_at: Time.now,
      player: player,
      challenge: challenge)
    trial.save
    player.get_point(challenge)
    io = Sinatra::RocketIO
    io.push :solved, {player_name: player.name, team_name: player.team.name, submitted_at: trial.submitted_at}.to_json
  rescue
    halt "Something wrong: #{$!}"
  end
  redirect "/admin/player/#{player.id}"
end
