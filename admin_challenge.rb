get '/admin/challenge/create' do
  @page_title = 'Create Challenge'
  admin_layout :admin_challenge_create
end

post '/admin/challenge/create' do
  @c = Challenge.create(params['name'], params['flag'], params['genre'], params['point'], params['desc'])
  if params['attachments']
    params['attachments'].each do |attach|
      buf = attach[:tempfile].read
      name = CGI.escape(attach[:filename].gsub(/[\r\n\f]/, ''))
      at = Attachment.new(
        name: name,
        type: attach[:type],
        hash: Digest::SHA1.hexdigest(buf),
        content: buf.to_sequel_blob
      )
      at.save
      @c.add_attachment(at)
    end
  end
  @c.save
  @page_title = 'Challenge Created!'
  admin_layout :admin_challenge_created
end

get '/admin/challenge/list' do
  @challenges = Challenge.all

  @page_title = 'List of Challenges'
  admin_layout :admin_challenge_list
end

get '/admin/challenge/:qnum' do
  qnum = Integer(params[:qnum]) rescue -1
  @challenge = Challenge.find(id: qnum)
  halt "Challenge (ID: #{qnum}) was not found." unless @challenge

  @page_title = "Admin - #{@challenge.name}"
  admin_layout :admin_challenge_individual
end

get '/admin/challenge/edit/:qnum' do
  qnum = Integer(params[:qnum]) rescue -1
  @challenge = Challenge.find(id: qnum)
  halt "Challenge (ID: #{qnum}) was not found." unless @challenge
  @page_title = "Edit \"#{@challenge.name}\""
  admin_layout :admin_challenge_edit
end

post '/admin/challenge/edit/:qnum' do
  qnum = Integer(params[:qnum]) rescue -1
  @challenge = Challenge.find(id: qnum)
  halt "Challenge (ID: #{qnum}) was not found." unless @challenge
  @challenge.name = params['name']
  @challenge.flag = Digest::SHA1.hexdigest(params['flag'])
  @challenge.genre = params['genre']
  @challenge.point = params['point']
  @challenge.description = params['desc']
  if params['attachments']
    params['attachments'].each do |attach|
      buf = attach[:tempfile].read
      at = Attachment.new(
        name: attach[:filename],
        type: attach[:type],
        hash: Digest::SHA1.hexdigest(buf),
        content: buf.to_sequel_blob
      )
      at.save
      @challenge.add_attachment(at)
    end
  end
  @challenge.save

  @page_title = "#{@challenge.name} is Updated"
  admin_layout :admin_challenge_edited
end

get '/admin/challenge/delete/:qnum' do
  qnum = Integer(params[:qnum]) rescue -1
  @challenge = Challenge.find(id: qnum)
  halt "Challenge (ID: #{qnum}) is not found." unless @challenge
  @challenge.destroy

  redirect '/admin/challenge/list'
end
