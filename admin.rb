require 'digest/sha1'

helpers do
  def is_admin_authenticated?
    session.key?('admin') && session['admin'] == true
  end

  def admin_login
    session['admin'] = true
  end

  def admin_logout
    session.delete('admin')
  end

  def admin_layout(page)
    haml :admin_layout, :layout => :layout do
      haml page
    end
  end
end

before /(^\/admin$)|(^\/admin\/(?!(login|logout)))/ do
  if !is_admin_authenticated?
    session['redirect_to'] = request.path_info
    redirect '/admin/login'
  end
end

require './admin_challenge'
require './admin_player'
require './admin_team'
require './admin_announce'

get '/admin' do
  @page_title = 'Admin'
  admin_layout :admin_index
end

get '/admin/login' do
  if is_admin_authenticated?
    redirect '/admin'
  else
    @page_title = "#{settings.site_name} Admin Login"
    haml :admin_login
  end
end

post '/admin/login' do
  pass = params['password']
  redirect '/admin/login' unless pass
  if Digest::SHA256.hexdigest(pass) == AppSettings['admin_pass']
    admin_login
  end
  if session.key?('redirect_to')
    ret_path = session['redirect_to']
    session.delete('redirect_to')
    redirect ret_path
  else
    redirect '/admin'
  end
end

get '/admin/logout' do
  admin_logout
  redirect '/admin/login'
end
