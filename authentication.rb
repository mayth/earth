Warden::Manager.serialize_from_session do |id|
  Player.find(id: id)
end

Warden::Manager.serialize_into_session do |player|
  player.id
end

Warden::Strategies.add :login do
  def valid?
    params['name'] && params['password']
  end

  def authenticate!
    p = Player.authenticate_by_name(params['name'], params['password'])
    p ? success!(p) : fail!
  end
end

use Warden::Manager do |manager|
  manager.default_strategies :login
  manager.failure_app = Sinatra::Application
end

post '/unauthenticated' do
  'you are failed to login!'
end
