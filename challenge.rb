get '/challenge/:qnum_s' do
  qnum = Integer(params[:qnum_s]) rescue -1
  @challenge = Challenge.find(id: qnum)
  halt "Challenge (ID: #{qnum}) was not found." unless @challenge
  
  @page_title = "#{@challenge.name} (Challenge #{qnum})"
  @page_subtitle = "#{@challenge.genre} #{@challenge.point}"
  haml :challenge_individual
end

post '/challenge/:qnum_s' do
  if request.env['warden'].user
    player = request.env['warden'].user
  else
    halt ({result: 'error', status: 'You must log in!'}.to_json)
  end

  # Accepting flag?
  halt ({result: 'error', status: 'STOP: 0x00000298 STOPPED_BY_ADMIN (All players cannot submit any flags now.)'}.to_json) if File.exists?('./disable_submit')

  # Suspended Player
  if player.is_suspended
    halt ({result: 'error', status: "Your account is suspended now. Please be patient until your account is resumed. (Your account will be resumed at #{player.suspended_until})"}.to_json)
  end

  # Suspended Team
  if player.team.is_suspended
    halt ({result: 'error', status: "You cannot submit any flags because your team's account is suspended. (Your team's account will be resumed at #{player.team.suspended_until})"}.to_json)
  end

  # Limit Exceed
  if player.trials.select{|t| (Time.now - t.submitted_at) < AppSettings['flag_submit_burst']['time']}.size > AppSettings['flag_submit_burst']['count']
    unlock_time = Time.now + AppSettings['cooling_down']
    player.suspend(unlock_time)
    halt ({result: 'error', status: "You exceeds the limit! Your account is suspended for #{AppSettings['cooling_down']} seconds (Your account will be resumed at #{unlock_time})"}.to_json)
  end

  begin
    qnum = Integer(params[:qnum_s]) rescue -1
    challenge = Challenge.find(id: qnum)
    halt ({result: 'error', status: "Challenge ID #{qnum} is not found."}.to_json) unless challenge
    already_answered = player.already_answered?(challenge)
    is_accepted = challenge.is_correct?(params['flag'])
    halt ({result: 'error', status: 'Your flag is correct, but remember that you are already answered this problem.'}.to_json) if (already_answered && is_accepted)
    halt ({result: 'error', status: 'Your flag is correct, but you can\'t get point because your team member already answered this problem.'}.to_json) if (player.team.find_answered_member(challenge) && is_accepted)

    trial = Trial.try(player, qnum, params['flag'])
  rescue 
    halt ({result: 'error', status: $!}.to_json)
  end

  if trial.is_accepted
    player.get_point(challenge)
    io = Sinatra::RocketIO
    io.push :solved, {player_name: player.name, team_name: player.team.name, submitted_at: trial.submitted_at}.to_json
    {result: 'accept', gain: challenge.point, current: player.score}.to_json
  else
    {result: 'wrong'}.to_json
  end
end

get '/challenge/attachment/:id' do
  id = Integer(params[:id]) rescue -1
  @attachment = Attachment.find(id: id)
  halt "Attachment ID: #{id} is not found." unless @attachment
  content_type @attachment.type
  response['Content-Disposition'] = "attachment; filename=#{@attachment.name}"
  @attachment.content
end
