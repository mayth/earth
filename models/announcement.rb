class Announcement < Sequel::Model
  def self.truncate(content, options = {})
    opt = {}.merge(options || {})
    length = opt[:length] || 60
    omission = opt[:omission] || '...'
    if content.size >= length
      leading = content[0, length - omission.size] + omission
    else
      leading = content
    end
    leading
  end

  def self.create(title, content, lead = nil, options = {})
    opt = {}.merge(options)
    leading = self.truncate(content, options)
    announce = Announcement.new(
      title: title,
      content: content,
      leading: leading,
      created_at: Time.now)
    announce.save
    announce
  end
end
