class Trial < Sequel::Model
  many_to_one :player
  many_to_one :challenge

  def self.try(player, ch_num, flag)
    challenge = Challenge.find(id: ch_num)
    throw ArgumentError unless challenge
    t = Trial.new(
      submitted_flag: flag,
      is_accepted: challenge.is_correct?(flag),
      submitted_at: Time.now,
      player: player,
      challenge: challenge)
    t.save
    t
  end
end
