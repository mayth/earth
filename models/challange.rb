class Challenge < Sequel::Model
  one_to_many :attachments
  one_to_many :trials

  def self.create(name, flag, genre, point, description, created_at = nil)
    created_at ||= Time.now
    c = Challenge.new(
      name: name,
      flag: Digest::SHA1.hexdigest(flag),
      genre: genre,
      point: point,
      description: description,
      created_at: created_at)
    c.save
    c
  end

  def is_correct?(f)
    self.flag == Digest::SHA1.hexdigest(f)
  end
end
