class Team < Sequel::Model
  one_to_many :players
  one_to_many :scorelogs

  def self.create(name)
    team = Team.find(name: name)
    raise ArgumentError.new("Team name #{name} is already existed.") if team
    team = Team.new(
      name: name,
      is_suspended: false)
    team.save
    team
  end

  def score
    return 0 if (!self.players || self.players.empty?)
    self.players.map{|p| p.score}.inject(:+)
  end

  def find_answered_member(challenge)
    self.players.find {|p| p.already_answered?(challenge)}
  end

  def already_answered?(challenge)
    find_answered_member(challenge) ? true : false
  end

  def is_accepted?(challenge)
    self.players.any? {|p| p.already_answered?(challenge)}
  end

  def last_accept
    accepts = players.select{|p| p.last_accept}.map{|p| p.last_accept}
    if (accepts && accepts.any?)
      accepts.sort_by{|t| t.submitted_at}.reverse.first
    else
      nil
    end
  end

  def suspend(suspend_until)
    self.is_suspended = true
    self.suspended_until = suspend_until
    save
  end

  def resume
    self.is_suspended = false
    self.suspended_until = nil
    save
  end

  def check_suspend
    if suspended_until
      t = suspended_until < Time.now
      resume if t
      t
    end
  end
end
