require 'digest/sha2'

class Player < Sequel::Model
  many_to_one :team
  one_to_many :trials
  one_to_many :scorelogs

  def self.create(name, password, team)
    raise ArgumentError.new('name must not be empty or nil.') if (!name || name.empty?)
    raise ArgumentError.new('password must not be empty or nil.') if (!password || password.empty?)
    raise ArgumentError.new('given name contains unallowed character.') unless name =~ /^[a-zA-Z0-9_]+$/
    name_hash = Digest::SHA256.hexdigest(name)
    pass_hash = Digest::SHA256.hexdigest(password)
    hash = Digest::SHA256.hexdigest(name_hash + pass_hash + AppSettings['pass_salt'])
    p = Player.new(
      name: name,
      password: hash,
      score: 0,
      is_suspended: false)
    if team
      team.add_player(p)
      team.save
    end
    p.save
    p
  end

  def self.authenticate_by_name(name, password)
    p = Player.find(name: name)
    return false unless p
    name_hash = Digest::SHA256.hexdigest(name)
    pass_hash = Digest::SHA256.hexdigest(password)
    if p.password == Digest::SHA256.hexdigest(name_hash + pass_hash + AppSettings['pass_salt'])
      p
    else
      nil
    end
  end

  def already_answered?(challenge)
    trial = trials.select {|t| t.challenge == challenge}
    trial.any? {|t| t.is_accepted}
  end

  def last_accept
    accepts = trials.select{|t| t.is_accepted}
    if (accepts && accepts.any?)
      accepts.sort_by{|t| t.submitted_at}.reverse.first
    else
      nil
    end
  end

  def get_point(challenge)
    log = Scorelog.new(
      value: challenge.point,
      player: self,
      team: team,
      event: 'submit_flag',
      reason: "Accepted Challenge #{challenge.id}")
    self.score += challenge.point
    team.save if team
    save
    log.save
  end

  def adjust_point(value, event, reason = nil)
    log = Scorelog.new(
      value: value,
      event: event,
      reason: reason,
      player: self,
      team: team)
    self.score += value
    team.save if team
    save
    log.save
  end

  def suspend(suspend_until)
    self.is_suspended = true
    self.suspended_until = suspend_until
    save
  end

  def resume
    self.is_suspended = false
    self.suspended_until = nil
    save
  end

  def check_suspend
    if is_suspended
      t = suspended_until < Time.now
      resume if t
      t
    end
  end
end
