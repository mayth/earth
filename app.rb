require 'time'
require 'yaml'
require 'digest/sha1'
require 'digest/sha2'

DB = Sequel.connect("sqlite://db/#{ENV['RACK_ENV']}.db")
AppSettings = YAML.load_file('config.yml')

# require models
Dir.glob('./models/*.rb').each do |s|
  require_relative s
end

enable :sessions

require './authentication'
require './admin'
require './register'
require './challenge'
require './announce'

configure do
  # Set default values for templates
  set :haml, :format => :html5

  # load config.yml
  set :site_name, AppSettings['site_name']
  set :session_secret, AppSettings['session_secret'] || Digest::SHA1.hexdigest(Time.now.to_s)
  AppSettings['pass_salt'] ||= make_random_str(16)
end

helpers do
  def is_loggedin?
    request.env['warden'].user != nil
  end

  def make_random_str(length)
    (1..length).map{|i| rand(0x21..0x7e).chr}.join
  end
end

before do
  @challenges = Challenge.all
  @player = request.env['warden'].user
  @player.check_suspend if @player
  @player.team.check_suspend if @player
end

get '/' do
  @page_title = AppSettings['site_name']
  @page_subtitle = 'Welcome aboard'
  haml :index
end

get '/login' do
  @page_title = 'Login'
  haml :login
end

post '/login' do
  request.env['warden'].authenticate!
  redirect '/'
end

get '/logout' do
  request.env['warden'].logout
  redirect '/'
end

get '/rules' do
  @page_title = 'Rules'
  haml :rules
end

get '/ranking' do
  @page_title = 'Ranking'
  haml :ranking
end

get '/exists_player' do
  Player.find(name: params['name']) ? 'true' : 'false'
end
