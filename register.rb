get '/register' do
  redirect '/' if is_loggedin?
  @page_title = 'Register'
  haml :register
end

post '/register' do
  halt 'The given name contains unacceptable characters.' unless params['name'] =~ /^[0-9A-Za-z_]+$/
  if params['newteam'] && !params['newteam'].empty?
    halt "The given team name contains any unacceptable characters." unless params['name'] =~ %r!^[/\]\[a-zA-Z0-9_-]+$!
  end
  @new_player = Player.find(name: params['name'])
  halt "Name #{params['name']} is already used. Please go back and retry." if @new_player
  team = nil
  if params['newteam'] && !params['newteam'].empty?
    team = Team.create(params['newteam'])
  else
    begin
      team_id = Integer(params[:team])
    rescue
      halt 'The given team ID is invalid.'
    end
    team = Team.find(id: team_id)
  end

  halt "Team ID: #{team_id} is not found." unless team
  @new_player = Player.create(params['name'], params['password'], team)

  @page_title = 'Registered'
  haml :registered
end
