Sequel.migration do
  change do
    alter_table :players do
      set_column_not_null :team_id
    end
  end
end
