Sequel.migration do
  change do
    create_table :attachments do
      primary_key :id
      String :name, :null => false
      String :type, :null => false
      String :hash, :null => false, :fixed => true, :size => 40
      File :content, :null => false
      foreign_key :challenge_id
    end
  end
end
