Sequel.migration do
  change do
    create_table :trials do
      primary_key :id
      String :submitted_flag, null: false
      Boolean :is_accepted, null: false
      DateTime :submitted_at, null: false
      foreign_key :player_id, null: false
      foreign_key :challenge_id, null: false
    end
  end
end
