Sequel.migration do
  change do
    create_table :players do
      primary_key :id
      String :name, null: false
      Integer :score, null: false
      String :password, null: false, fixed: true, size: 64
      Boolean :is_suspended, null: false
      DateTime :suspended_until
      foreign_key :team_id
    end
  end
end
