Sequel.migration do
  change do
    create_table :challenges do
      primary_key :id
      String :name, :null => false
      String :flag, :null => false, :fixed => true, :size => 40
      String :genre, :null => false
      Integer :point, :null => false
      String :description, :null => false
      DateTime :created_at, :null => false
    end
  end
end
