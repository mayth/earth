Sequel.migration do
  change do
    create_table :teams do
      primary_key :id
      String :name, :null => false
      Boolean :is_suspended, null: false
      DateTime :suspended_until
    end
  end
end
