Sequel.migration do
  change do
    create_table :announcements do
      primary_key :id
      String :title, null: false
      String :leading, null: false
      String :content, null: false
      DateTime :created_at, null: false
    end
  end
end
