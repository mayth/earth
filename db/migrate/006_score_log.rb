Sequel.migration do
  change do
    create_table :scorelogs do
      primary_key :id
      Integer :value, null: false
      String :event, null: false
      String :reason
      foreign_key :player_id, null: false
      foreign_key :team_id
    end
  end
end
