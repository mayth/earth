earth
=====

Abstrct
=======
__earth__ is a CTF(Capture the Flag) score server.

Requirements
============
* Ruby (Written with Ruby 2.0.0-p0)
* Bundler

Setup
=====
1. Install `bundler` if it is not installed.
2. Execute `bundle install --path ./vendor/bundle` (You can modify the install path. I __strongly__ recommend to install gems locally.)
3. Execute `bundle exec sequel -m ./db/migrate sqlite://db/development.db` (You can use other adapters, or other path to a database file. If you use any other adapters or database file, you MUST modify connection string in app.rb.) Note that the default path for SQLite database is `db/(ENVIRONMENT).db`. For example, `db/development.db`, `db/production.db`.
4. Modify 'config.yml'.
    * `site_name` will be used in the header and the title.
    * Modify `admin_name` from `admin`.
    * `admin_pass` can be made by this command: `ruby -rdigest/sha2 -e 'puts Digest::SHA256.hexdigest("YOUR_PASSWORD_HERE")'`
    * `session_secret` can be made by this command: `ruby -rdigest/sha2 -e 'puts Digest::SHA256.hexdigest(Time.now.to_s)'`. Of course, you can use your favorite string instead of the SHA-256 digest of the time.
4. Execute `bundle exec rackup`, and the application starts.
5. Access to http://localhost:9292 with the browser you love. If you see '(your site_name)  Welcome aboard', the application runs well; otherwise, check your console. The application server puts the log to standard output.
6. To add some challenges or players, or to check the statistics, access to /admin and log in.

Configuration
-------------
The values in parenthesis '(' ')' means its default value.

site_name ('earth ctf server')
:The name of your CTF contest.

admin_name ('admin')
:This is used for logging in to admin page.

admin_pass (SHA-256 hash of 'password')
:The password for the admin user, hashed by SHA-256.

session_secret ('0e202b83de0bf63f101b2d19bf1160e528d906')
:The key used for session_id. If you change this and restart the server, all sessions are disabled. It means all users are forced to be logged out.

num_recent_trials (10)
:A number of items to show in 'Recent Trials' section in admin page.

flag_submit_burst (10/10)
:Specify the limitation for submitting flags. If [count] submits are performed in [time] seconds, the user's account will be suspended.

cooling_down (900)
:Specify suspending time when an user exceeds the limit configured above. [unit: seconds]

pass_salt ('*BjXL{C4>nX\"T`on<')
:Password salt for user's password (This will not affect to admin's password.) Changing this makes all users' password disabled.

Other Features
--------------

### Stop Flag Submitting

You can stop all players' submitting flag by making the file named `disable_submit` in the application root (the directory that contains 'app.rb'). The easiest way is `touch disable_submit`.
The application checks if the file `disable_submit` is existed or not. If it exists, the application rejects any flags.
The content of `disable_submit` has no effects. It can be empty. It is important that it is existed or not.
