get '/admin/announce/create' do
  @page_title = 'Create Announcement'
  admin_layout :admin_announce_create
end

post '/admin/announce/create' do
  title = params['title']
  halt 'title must not be empty or nil' if (!title || title.empty?)
  content = params['content']
  halt 'content must not be empty or nil' if (!content || content.empty?)
  leading = params['leading'].empty? ? nil : params['leading']
  begin
    announce = Announcement.create(title, content, leading)
  rescue
    halt "Something went wrong... #{$!}"
  end

  io = Sinatra::RocketIO
  io.push :new_announcement, {id: announce.id, title: announce.title, created_at: announce.created_at, content: announce.content, leading: announce.leading}.to_json
  redirect '/admin/announce/list'
end

get '/admin/announce/list' do
  @page_title = 'List of Announcements'
  admin_layout :admin_announce_list
end

get '/admin/announce/update/:announce_id' do
  announce_id = Integer(params[:announce_id]) rescue -1
  @announce = Announcement.find(id: announce_id)
  halt "Announcement ID: #{announce_id} is not found." unless @announce

  @page_title = @announce.title
  @page_subtitle = "Update Announcement"
  admin_layout :admin_announce_update
end

post '/admin/announce/update/:announce_id' do
  announce_id = Integer(params[:announce_id]) rescue -1
  announce = Announcement.find(id: announce_id)
  halt "Announce ID: #{announce_id} is not found." unless announce
  title = params['title']
  halt 'title must not be empty or nil' if (!title || title.empty?)
  content = params['content']
  halt 'content must not be empty or nil' if (!content || content.empty?)
  leading = params['leading'].empty? ? nil : params['leading']
  begin
    announce.title = title
    announce.content = content
    announce.leading = Announcement.truncate(content)
    announce.save
  rescue
    halt "Something went wrong... #{$!}"
  end

  io = Sinatra::RocketIO
  io.push :update_announcement, {id: announce.id, title: announce.title, content: announce.content, leading: announce.leading, created_at: announce.created_at}.to_json
  redirect "/admin/announce/#{announce.id}"
end

post '/admin/announce/delete/:announce_id' do
  content_type :json
  announce_id = Integer(params[:announce_id]) rescue -1
  announce = Announcement.find(id: announce_id)
  halt ({result: 'error', reason: "Announce ID: #{announce_id} is not found."}.to_json) unless announce
  begin
    announce.destroy
  rescue
    halt ({result: 'error', reason: "Something happens... #{$!}"}.to_json)
  end
  io = Sinatra::RocketIO
  io.push :delete_announcement, {id: announce.id}.to_json
  {result: 'success'}.to_json
end

get '/admin/announce/:announce_id' do
  announce_id = Integer(params[:announce_id]) rescue -1
  @announce = Announcement.find(id: announce_id)
  halt "Announcement ID: #{announce_id} is not found." unless @announce

  @page_title = @announce.title
  @page_subtitle = "Announcement Detail"
  admin_layout :admin_announce_individual
end
