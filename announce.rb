get '/announce/list' do
  @page_title = 'Announcements'
  haml :announce_list
end

get '/announce/latest' do
  num = params['num']
  num = num.empty? ? 10 : num.to_i
  res_array = []
  Announcement.order(Sequel.desc(:created_at)).take(num).each do |announce|
    res_array << {id: announce.id, title: announce.title, leading: announce.leading, content: announce.content, created_at: announce.created_at}
  end
  content_type :json
  res_array.to_json
end

get '/announce/:announce_id' do
  announce_id = Integer(params[:announce_id]) rescue -1
  @announce = Announcement.find(id: announce_id)
  halt "Announcement ID: #{announce_id} is not found." unless @announce

  @page_title = @announce.title
  @page_subtitle = 'Announcement'
  haml :announce_individual
end
